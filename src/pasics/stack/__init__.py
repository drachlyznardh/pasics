
class ImmutableStack:
    class _Empty:
        def __len__(self): return 0
        def __iter__(self): return self
        def __next__(self): raise StopIteration
        def __enter__(self): return self
        def __exit__(self, exc_type, exc_value, traceback): return False
        def top(self): raise IndexError('stack index out of range')
        def push(self, value): return ImmutableStack(value, self)
        def pop(self): raise IndexError('pop from empty stack')
    Empty = _Empty()

    def __init__(self, value, previous):
        if previous is None: raise ValueError('previous stack is None')
        if not isinstance(previous, (ImmutableStack, ImmutableStack._Empty)): raise TypeError('target is not a stack')
        self._value = value
        self._previous = previous
        self._length = 1 + len(previous)
    def __len__(self): return self._length
    def __enter__(self): return self
    def __exit__(self, exc_type, exc_value, traceback): return False
    def __iter__(self):
        class Iterator:
            def __init__(self,link): self._link = link
            def __iter__(self): return self
            def __next__(self):
                if self._link is ImmutableStack.Empty: raise StopIteration
                value = self._link._value
                self._link = self._link._previous
                return value
        return Iterator(self)
    def push(self, value): return ImmutableStack(value, self)
    def pop(self): return self._previous

