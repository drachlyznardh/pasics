
class ImmutableLinkedList:
    class Empty:
        def __init__(self): pass
        def __len__(self): return 0
        def __iter__(self): return self
        def __next__(self): raise StopIteration
        def head(self): return None
        def remove(self): raise NotImplementedError
        def apppend(self, value): return Link(value, self)

    class Link:
        def __init__(self, value, following):
            self._value = value
            self._following = following
        def head(self): return self._value

    def __init__(self):
        global empty
        if empty is None: empty = self.Empty()
        self._root = empty
        self._length = 0
    def __enter__(self): return self
    def __exit__(self, exc_type, exc_value, traceback): return False

    def __iter__(self):
        class Iterator(self):
            def __init__(self, link): self._link = link
            def __iter__(self): return self
            def __next__(self):
                global empty
                if self._link is empty: raise StopIteration
                value = self._link._value
                self._link = self._link._following
                return value
        return empty if self._root is empty else Iterator(self._root)

    def __len__(self): return self._length

    def head(self): return self._root.head()

    def append(self, value):
        global empty
        if self._root is empty: self._root = self.Link(value, empty)
        else:
            lastLink = self._root
            while lastLink._following is not empty: lastLink = lastLink._following
            lastLink._following = self.Link(value, empty)
        self._length += 1
        return self

    def remove(self):
        global empty
        if self._root is empty: return None
        link = self._root
        self._root = link._following
        self._length -= 1
        return self

empty = ImmutableLinkedList.Empty()

