
NAME=pasics
VERSION=1.0.0

SETUP=python3 setup.py
PIP=pip3
TGZ=dist/$(NAME)-$(VERSION).tar.gz
RPM=$(NAME)-$(VERSION)-1.noarch.rpm

OUTSRCDIR=src/
RESDIR=res/

SRCS=$(shell find $(OUTSRCDIR) -name \*.py )
RESS=$(shell find $(RESDIR) -name \*.py | xargs printf "%s," )

all: quick-test

setup.py: %: %.in Makefile
	@sed -e 's;@NAME@;$(NAME);g; s;@VERSION@;$(VERSION);g; s;@OUTDIR@;$(OUTSRCDIR);g;' < $< > $@

$(TGZ): setup.py $(SRCS)
	@$(SETUP) sdist

$(RPM): $(TGZ)
	@potter -v start bake
	@potter -v bake -b $<

sdist: $(TGZ)
bdist_rpm: $(RPM)

quick-test:
	@$(PIP) install --user .
	@cd $(RESDIR) && pytest

test: $(RPM)
	@potter -v start test
	@potter -v test -up python3-pytest -c pytest-3 -r$(RESS) $(RPM)

clean: setup.py
	$(SETUP) clean

veryclean:
	@rm -rf setup.py dist/ $(RPM)

.PHONY: clean veryclean quick-test test

