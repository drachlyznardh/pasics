
import pytest
from pasics.stack import ImmutableStack
S = ImmutableStack.Empty

"An empty stack has no elements"
def test_00(): assert len(S) == 0

"There's no getting elements from an empty stack"
def test_01():
    with pytest.raises(IndexError):
        S.top()

"By adding an element, a stack grows longer"
def test_02():
    def helper(inStack):
        outStack = inStack.push(0)
        assert len(outStack) > len(inStack)
        return outStack
    with S as l:
        for e in range(3): l = helper(l)

"By removing an element, a stack grows shorter"
def test_03():
    def helper(inStack):
        outStack = l.pop()
        assert len(outStack) < len(inStack)
        return outStack
    with S.push(0).push(1).push(3) as l:
        for e in range(len(l)): l = helper(l)

"An empty stack cannot be iterated"
def test_04():
    for e in S: assert False

"A stack with elements can be iterated"
def test_05():
    with S as l:
        for e in range(3):
            l = l.push(e)
            for t in l:
                if t == len(l) -1: break
            else: assert False

