
from pasics.list import ImmutableLinkedList as L

"An empty list has no elements"
def test_00(): assert len(L()) == 0

"There's no getting elements from an empty list"
def test_01(): assert L().head() == None

"By adding an element, a list grows longer"
def test_02():
    def helper(inList):
        outList = inList.append(0)
        assert len(outList) > len(inList)
        return outList
    with L() as l:
        for e in range(3): l = helper(l)

"By removing an element, a list grows shorter"
def test_03():
    def helper(inList):
        outList = l.remove()
        assert len(outList) < len(inList)
        return outList
    with L().append(0).append(1).append(3) as l:
        for e in range(len(l)): l = helper(l)

"An empty list cannot be iterated"
def test_04():
    for e in L(): assert False

"A list with elements can be iterated"
def test_05():
    with L() as l:
        for e in range(3):
            l = l.append(e)
            for t in l:
                print('Element {} found!'.format(t))
                if t == len(l) -1: break
            else: assert False

